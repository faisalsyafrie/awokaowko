package nugroho.dimas.alo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cn.pedant.SweetAlert.SweetAlertDialog
import kotlinx.android.synthetic.main.activity_welcome.*
import nugroho.dimas.alo.login.login
import nugroho.dimas.alo.user.user_index

class welcome : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        next.setOnClickListener {
            trus("Apakah Anda seorang Admin?")
        }
    }

    fun trus(string: String?) {
        SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
            .setTitleText("Hello :)")
            .setContentText(string)
            .setConfirmText("Iya")
            .setCancelText("Tidak")
            .setConfirmClickListener { sDialog ->
                sDialog.dismissWithAnimation()
                val aa = Intent(this,login::class.java)
                startActivity(aa)
            }
            .setCancelClickListener { sDialog ->
                sDialog.dismissWithAnimation()
                val aa = Intent(this,
                    user_index::class.java)
                startActivity(aa)
            }
            .show()
    }
}
