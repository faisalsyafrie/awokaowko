package nugroho.dimas.alo.serviceNotifikasi

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import nugroho.dimas.mvvmfirebase.R
import nugroho.dimas.mvvmfirebase.user.user_index
import kotlin.random.Random
private const val  Chanel_id = "pesanku"
class serviceNotif : FirebaseMessagingService() {
    companion object {
        var sharedpref: SharedPreferences? = null
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)

        val intent = Intent(this, user_index::class.java)
        val notikasiManeger = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notifikasionId = Random.nextInt()

//        deklar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createnotification(notikasiManeger)
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingInten = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val notifikasi = NotificationCompat.Builder(this, Chanel_id)
            .setContentTitle(message.data["title"])
            .setContentText(message.data["message"])
            .setSmallIcon(R.drawable.ic_local_grocery_store_black_24dp)
            .setAutoCancel(true)
            .setContentIntent(pendingInten)
            .build()


        notikasiManeger.notify(notifikasionId, notifikasi)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createnotification(notificationManager: NotificationManager) {
        val chanelname = "chanelname"
        val chanel =
            NotificationChannel(Chanel_id, chanelname, NotificationManager.IMPORTANCE_HIGH).apply {
                description = "My Channel Deskripsi"
                enableLights(true)
                lightColor = Color.GREEN
                enableVibration(true)
            }
        notificationManager.createNotificationChannel(chanel)
    }
}