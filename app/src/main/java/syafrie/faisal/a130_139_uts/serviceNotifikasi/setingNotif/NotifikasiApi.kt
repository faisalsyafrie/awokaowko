package nugroho.dimas.alo.serviceNotifikasi.setingNotif

import nugroho.dimas.alo.serviceNotifikasi.setingNotif.Constan.Companion.Content_type
import nugroho.dimas.alo.serviceNotifikasi.setingNotif.Constan.Companion.SERVER_KEY
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface NotifikasiApi {
    @Headers("Authorization:key=$SERVER_KEY","Content-Type:$Content_type")
    @POST("fcm/send")
    suspend fun posNotif(
        @Body notification :pushNotifikasi
    ): Response<ResponseBody>
}