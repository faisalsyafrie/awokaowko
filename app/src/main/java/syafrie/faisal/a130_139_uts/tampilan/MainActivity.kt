package nugroho.dimas.alo.tampilan

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.tambah_data.*
import kotlinx.android.synthetic.main.tambah_data.view.*
import nugroho.dimas.mvvmfirebase.R
import nugroho.dimas.mvvmfirebase.UploadHelper
import nugroho.dimas.mvvmfirebase.ricykleviewAdapter.MainAdapter
import nugroho.dimas.mvvmfirebase.viewModel.MainViewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var adapter: MainAdapter
    private val viewModel by lazy { ViewModelProviders.of(this).get(MainViewModel::class.java) }
    //    lateinit var db : FirebaseFirestore
    lateinit var uri: Uri

    lateinit var storage: StorageReference
    lateinit var db: CollectionReference
    lateinit var UploadHelper: UploadHelper
    var imStr = ""
    val F_ID = "id"
    val F_Nama = "nama"
    val F_harga = "harga"
    val F_stock = "stock"
    val F_imageurl = "imageUrl"
    val F_status = "status"
    val F_ket = "keterangan"

    var filename = ""

    lateinit var viewDial: View
    //    lateinit var alFile : ArrayList<HashMap<String,Any>>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewDial = LayoutInflater.from(this).inflate(R.layout.tambah_data, null)
        adapter = MainAdapter(this, this)
        rv.layoutManager = GridLayoutManager(this, 2)
        rv.adapter = adapter

        tambah.setOnClickListener(this)
        UploadHelper = UploadHelper(this)

        uri = Uri.EMPTY


    }

    override fun onStart() {
        super.onStart()
        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("isi_row")
        db.addSnapshotListener { querySnapshot, firebaseFireStoreException ->
            if (firebaseFireStoreException != null) {
                firebaseFireStoreException.message?.let { Log.e("Firestore : ", it) }
            }
            observeData()
        }

    }

    fun observeData() {
        shimmer_view_container.startShimmer()
        viewModel.fetcUserData().observe(this, Observer { data ->
            shimmer_view_container.hideShimmer()
            shimmer_view_container.stopShimmer()
            shimmer_view_container.visibility = View.GONE
            adapter.setListData(data)
            adapter.notifyDataSetChanged()
        })
    }

    //    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK)){
//            if(data != null) {
//                uri = data.data!!
//                txSelectedFile.setText(uri.toString())
//            }
//        }
//    }
//method upload
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((resultCode == Activity.RESULT_OK) && (requestCode == UploadHelper.getRcGalery())) {
            if (data != null) {
                imStr = UploadHelper.getBitmapToString(data!!.data, viewDial.gambar)
                uri = data.data!!
                viewDial.isigambar.setText(uri.toString())
            }
        }
    }

    fun sukses(string: String?) {
        SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
            .setTitleText("Barang berhasil ditambahkan :)")
            .setContentText(string)
            .setConfirmText("OK")
            .setConfirmClickListener { sDialog ->
                sDialog.dismissWithAnimation()
            }
            .show()
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tambah -> {
                val dialog = AlertDialog.Builder(this)
                    .setView(viewDial)
                val LihatDial = dialog.show()
                viewDial.batal.setOnClickListener {
                    LihatDial.dismiss()
//                    var namaPS = viewDial.psNam.text.toString()
                }

                viewDial.edBar.setOnClickListener {
                    LihatDial.dismiss()
                    var fileName = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                    val fileref = storage.child(fileName + ".jpg")
                    fileref.putFile(uri)
                        .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                            return@Continuation fileref.downloadUrl
                        })
                        .addOnCompleteListener { task ->
                            val hm = HashMap<String, Any>()
                            hm.put(F_ID, fileName)
                            hm.put(F_Nama, viewDial.namabar.text.toString())
                            hm.put(F_harga, viewDial.hargabar.text.toString())
                            hm.put(F_imageurl, task.result.toString())
                            hm.put(F_stock, viewDial.Stokbar.text.toString())
                            hm.put(F_status, viewDial.spn.selectedItem.toString())
                            hm.put(F_ket, viewDial.keterangan.text.toString())
                            db.document(fileName).set(hm).addOnSuccessListener {
                                sukses(viewDial.namabar.text.toString())
                            }
                        }
                }
                viewDial.gambar.setOnClickListener {
                    val intent = Intent()
                    intent.setType("image/*")
                    intent.setAction(Intent.ACTION_GET_CONTENT)
                    startActivityForResult(intent, UploadHelper.getRcGalery())


                }

            }
        }
    }


}
