package nugroho.dimas.alo.tampilan

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.pesan_bc.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import nugroho.dimas.mvvmfirebase.R
import nugroho.dimas.mvvmfirebase.serviceNotifikasi.setingNotif.RetrofitInstance
import nugroho.dimas.mvvmfirebase.serviceNotifikasi.setingNotif.notifikasiData
import nugroho.dimas.mvvmfirebase.serviceNotifikasi.setingNotif.pushNotifikasi
const val Topic = "/topics/myTopic"

class menu : AppCompatActivity(), View.OnClickListener {
    var fbaut= FirebaseAuth.getInstance()
    lateinit var viewDial : View
    val Tag = "openfcm"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        val root = findViewById<LinearLayout>(R.id.pesanku)
        viewDial = layoutInflater.inflate(R.layout.pesan_bc, root, false)
        menu1.setOnClickListener(this)
        pes.setOnClickListener(this)
        menu3.setOnClickListener(this)
        logout.setOnClickListener(this)

        FirebaseMessaging.getInstance().subscribeToTopic(Topic)

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.menu1->{
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
            R.id.pes->{
                val dialog = AlertDialog.Builder(this)
                    .setView(viewDial)
                val LihatDial = dialog.show()
                viewDial.batal.setOnClickListener {
                    LihatDial.hide()

//                    var namaPS = viewDial.psNam.text.toString()
                }

                viewDial.kirimbc.setOnClickListener {
                    LihatDial.hide()

                    val judulkirim = viewDial.judulpes.text.toString()
                    val pesankirim = viewDial.isipes.text.toString()
                    if (judulkirim.isNotEmpty() && pesankirim.isNotEmpty()){
                        pushNotifikasi(
                            notifikasiData(judulkirim,pesankirim),
                            Topic
                        ).also {
                            sendNotifikasi(it)
                        }
                    }
                }
            }
            R.id.menu3->{
                Toast.makeText(this,"menu ke 3", Toast.LENGTH_SHORT).show()
            }
            R.id.logout->{
               logout1("Ingin keluar dari Aplikasi?")
            }
        }
    }


    fun logout1(string: String?) {
        SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
            .setTitleText("Warning")
            .setContentText(string)
            .setConfirmText("Iya")
            .setCancelText("Tidak")
            .setConfirmClickListener { sDialog -> sDialog.dismissWithAnimation()
                fbaut.signOut()
                finish()
            }
            .setCancelClickListener{ sDialog -> sDialog.dismissWithAnimation()
            }
            .show()
    }

    private fun sendNotifikasi(notifikasi: pushNotifikasi) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val response = RetrofitInstance.api.posNotif(notifikasi)
            if (response.isSuccessful) {
                Log.d(Tag, "Response: ${Gson().toJson(response)}")
            } else {
                Log.e(Tag, response.errorBody().toString())
            }
        } catch (e: Exception) {
            Log.e(Tag, e.toString())
        }
    }
}
