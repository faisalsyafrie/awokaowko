package nugroho.dimas.alo.tampilan

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import cn.pedant.SweetAlert.SweetAlertDialog
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_kelola_data.*
import nugroho.dimas.mvvmfirebase.R
import nugroho.dimas.mvvmfirebase.UploadHelper
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class kelola_data : AppCompatActivity() {
    lateinit var storage: StorageReference
    lateinit var db: CollectionReference
    lateinit var UploadHelper: UploadHelper
    var imStr = ""
    val F_ID = "id"
    val F_Nama = "nama"
    val F_harga = "harga"
    val F_stock = "stock"
    val F_imageurl = "imageUrl"
    val F_status = "status"
    val F_ket = "keterangan"
    lateinit var uri: Uri
    var id = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kelola_data)
        uri = Uri.EMPTY
        UploadHelper = UploadHelper(this)
        val aa = intent
        id = aa.getStringExtra("id")
        var nama = aa.getStringExtra("nama")
        var harga = aa.getStringExtra("harga")
        var foto = aa.getStringExtra("imageurl")
        var stock = aa.getStringExtra("stock")
        var status = aa.getStringExtra("status")
        var keterangan = aa.getStringExtra("keterangan")

        if (status == "Tersedia") {
            spnkel.getItemAtPosition(0)
        }
        if (status == "Habis") {
            spnkel.getItemAtPosition(1)
        }
        if (status == "Dalam Pemesanan") {
            spnkel.getItemAtPosition(2)
        }
        namakel.setText(nama)
        hargakel.setText(harga)
        Glide.with(this).load(foto).into(gambarkel)
        Stokbarkel.setText(stock)
        keterangankel.setText(keterangan)

        edit.setOnClickListener {
            var fileName = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
            if (foto != fileName ) {
                val hm = HashMap<String, Any>()
                hm.put(F_Nama, namakel.text.toString())
                hm.put(F_harga, hargakel.text.toString())
                hm.put(F_imageurl, foto.toString())
                hm.put(F_stock, Stokbarkel.text.toString())
                hm.put(F_status, spnkel.selectedItem.toString())
                hm.put(F_ket, keterangankel.text.toString())
                db.document(id).update(hm).addOnSuccessListener {
                    sukses(namakel.text.toString())
                }
            } else {

                var fileName = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                val fileref = storage.child(fileName + ".jpg")
                fileref.putFile(uri)
                    .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                        return@Continuation fileref.downloadUrl
                    })
                    .addOnCompleteListener { task ->
                        val hm = HashMap<String, Any>()
                        hm.put(F_Nama, namakel.text.toString())
                        hm.put(F_harga, hargakel.text.toString())
                        hm.put(F_imageurl, task.result.toString())
                        hm.put(F_stock, Stokbarkel.text.toString())
                        hm.put(F_status, spnkel.selectedItem.toString())
                        hm.put(F_ket, keterangankel.text.toString())
                        db.document(id).update(hm).addOnSuccessListener {
                            sukses(namakel.text.toString())
                        }
                    }
            }
        }
        hapus.setOnClickListener {
            hap("Apakah Anda yakin menghapus data?")
        }

    }

    fun hap(string: String?) {
        SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
            .setTitleText("Warning")
            .setContentText(string)
            .setConfirmText("Iya")
            .setCancelText("Tidak")
            .setConfirmClickListener { sDialog ->
                sDialog.dismissWithAnimation()
                db.whereEqualTo(F_ID, id).get().addOnSuccessListener { result ->
                    for (doc in result) {
                        db.document(doc.id).delete()
                            .addOnSuccessListener {
                                sukseshap(namakel.text.toString())

                            }
                    }
                }
            }
            .setCancelClickListener { sDialog ->
                sDialog.dismissWithAnimation()
            }
            .show()
    }

    fun sukses(string: String?) {
        SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
            .setTitleText("Update berhasil :)")
            .setContentText(string)
            .setConfirmText("OK")
            .setConfirmClickListener { sDialog ->
                sDialog.dismissWithAnimation()
                finish()
            }
            .show()
    }

    fun sukseshap(string: String?) {
        SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
            .setTitleText("Hapus berhasil!")
            .setContentText(string)
            .setConfirmText("OK")
            .setConfirmClickListener { sDialog ->
                sDialog.dismissWithAnimation()
                finish()
            }
            .show()
    }


    override fun onStart() {
        super.onStart()
        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("isi_row")
        db.addSnapshotListener { querySnapshot, firebaseFireStoreException ->
            if (firebaseFireStoreException != null) {
                firebaseFireStoreException.message?.let { Log.e("Firestore : ", it) }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((resultCode == Activity.RESULT_OK) && (requestCode == UploadHelper.getRcGalery())) {
            if (data != null) {
                imStr = UploadHelper.getBitmapToString(data!!.data, gambarkel)
                uri = data.data!!
                isigambarkel.setText(uri.toString())
            }
        }
    }


}
