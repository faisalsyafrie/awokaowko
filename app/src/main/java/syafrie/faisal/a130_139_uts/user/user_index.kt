package nugroho.dimas.alo.user

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_main.*
import nugroho.dimas.alo.R
import nugroho.dimas.alo.ricykleviewAdapter.AdapUser
import nugroho.dimas.alo.viewModel.MainViewModel

class user_index : AppCompatActivity() {

    private lateinit var adapter: AdapUser
    private val viewModel by lazy { ViewModelProviders.of(this).get(MainViewModel::class.java) }
    lateinit var uri: Uri
    lateinit var storage: StorageReference
    lateinit var db: CollectionReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_index)
        adapter = AdapUser(this, this)
        rv.layoutManager = GridLayoutManager(this, 2)
        rv.adapter = adapter

    }

    fun observeData() {
        shimmer_view_container.startShimmer()
        viewModel.fetcUserData().observe(this, androidx.lifecycle.Observer {    data ->
            shimmer_view_container.hideShimmer()
            shimmer_view_container.stopShimmer()
            shimmer_view_container.visibility = View.GONE
            adapter.setListData(data)
            adapter.notifyDataSetChanged()
        })
    }

    override fun onStart() {
        super.onStart()
        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("isi_row")
        db.addSnapshotListener { querySnapshot, firebaseFireStoreException ->
            if (firebaseFireStoreException != null) {
                firebaseFireStoreException.message?.let { Log.e("Firestore : ", it) }
            }
        }
        observeData()

    }



}
