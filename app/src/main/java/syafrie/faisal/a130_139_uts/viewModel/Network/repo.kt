package nugroho.dimas.alo.viewModel.Network

import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.FirebaseFirestore
import nugroho.dimas.alo.data_item.isi_row

class repo {
    fun getData(): LiveData<MutableList<isi_row>> {
        val mutableData = MutableLiveData<MutableList<isi_row>>()
        FirebaseFirestore.getInstance().collection("isi_row").get().addOnSuccessListener { result ->
                val listData = mutableListOf<isi_row>()
                for (document in result) {
                    val imageUrl = document.getString("imageUrl")
                    val nama = document.getString("nama")
                    val harga = document.getString("harga")
                    val stock = document.getString("stock")
                    val status = document.getString("status")
                    val ket = document.getString("keterangan")
                    val id = document.getString("id")
                    val isi_row = isi_row(imageUrl.toString(), nama.toString(), stock.toString(),harga.toString(),status.toString(),ket.toString(),id.toString())
                    listData.add(isi_row)
                }
                mutableData.value = listData
            }
        return mutableData
    }
}
